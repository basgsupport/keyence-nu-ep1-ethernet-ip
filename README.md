﻿---

## About

This project is using an Keyence Ethernet IP communication unit NU-EP1 with a pressure sensor AP-N10 to communicate with TwinCAT to obtain pressure values as well as setpoint values from the pressure sensor.
I have also attach a simple Ethernet IP connection guide-"Ethernet IP Master Quick Start" in case you need any assistance. If you have any feedback, contact me at zs.tan@beckhoff.com.sg
---


## Pre-requisite

1.Identified the Port that the Ethernet IP Device is connected to .
For Ethernet IP Devices, it can only be scanned by using Ethernet IP Master 
	i.Our controller or laptop Intel PCI ethernet Adapator can be used as a port to scan for Ethernet IP Devices Via the use of 
		a.TF6281 | TC3 EtherNet/IP Master 
			#.The TwinCAT EtherNet/IP Master is a supplement that turns any PC-based controller with an Intel® chipset and the real-time Ethernet driver developed by Beckhoff into an EtherNet/IP master. Through this supplement, the Ethernet interface becomes an EtherNet/IP master. The product can be used on all PC controllers and Embedded PC controllers with an Intel® chipset. 
	ii.Alternatively, we can convert EtherCAT communication to Ethernet IP communication through the use of Ethernet/IP master terminal EL6652.

2. Download NU-EP EDS File from keyence website. "Keyence_2501_0101.eds"
3. Download NU-EP Instruction manual from keyence website."AS_96413_NU-EP1_Communication Unit"
	i. This will help you to understand the variable input and output mapping to twincat.


---

## How to use

Refer to NU-EP instruction manual that I have included into the folder.

---

## Help

Contact support@beckhoff.com.sg


